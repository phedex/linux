" VI profile

" NOTE
" map: map key in view mode; imap: map key in insert mode
" imap: insert mode [C-O switch to exec command & return to insert mode]

" SET TERMINAL
" in case of problems with arrows keys
" set term=linux

" REPLACE
  map r R

" COMMAND ALIAS
" cabbrev x y

" CHARSET
  set encoding=utf-8
  set fileencoding=utf-8
  set termencoding=utf-8

" NO WORDWRAP - NO A CAPO
  set nowrap
  map <F10> :set wrap!<CR>

" VARIE
  set nosol

" UNMAP F1
  map  <F1> <nop>
  imap <F1> <nop>

" QUIT / FILE
  map  <F3> :q<CR>
  imap <F3> <nop>

" STATUS LINE
  set ls=2
  set ruler

" NO BACKUP FILE
  set nobackup

" LINE NUMBER - F2 to swap on/off
  set nonumber
  map  <F2> :set number!<CR>

" TOP - END file
  map  <F8>       gg
  imap <F8> <C-O> gg
  map  <F9>       G
  imap <F9> <C-O> G
" LEFT - RIGHT word
  map  q  b
  map  w  w

" BACKSPACE  [replace "^?" with CTRL-v,BACKSPACE]
" set t_kb=^?
  set t_kb=
  set backspace=eol,indent,start

" SEARCH
" IGNORE CASE Upper/lower
  set  ignorecase
" NO WRAP to TOP of file
" set  nowrapscan

" turn on INCREMENTAL SEARCHING
  set  incsearch

" CURRENT LINE at TOP of window
  map  <F4>      z<CR>
  imap <F4> <C-O>z<CR>

" DUPLICATE line
  "ap  <F7> Yp

" HIGHLIGHT - F5/F7 highlight/cursorline
  set  hls  "nohls
  map  <F5> :set hls!<CR>
  map  <F7> :set cursorline!<CR>

" SYNTAX HIGHLIGHT - F6
  syntax on
  map  <F6> :if exists("g:syntax_on") <Bar> syntax off <Bar> else <Bar> syntax enable <Bar> endif <CR>

" COLOR SCHEMA
  colorscheme default  " clear: pablo
  map <C-d> :colorscheme default<CR>
  map <C-w> :colorscheme pablo<CR>

" HIGHLIGHT COLOR
  highlight modeMsg cterm=bold ctermfg=black ctermbg=white
  highlight Search  cterm=bold ctermfg=black ctermbg=red

" INDENT
  "set  noautoindent
  "set  nocindent
  filetype indent off
  filetype plugin indent off

" CHANGE alias
  map  ss  :1,$s,,,g

" view TAB,CR char
" set  list

" Undo CTRL-Z
  map  <C-Z>      u
  imap <C-Z> <C-O>u
" Redo CTRL-Y
  map  <C-Y>      <C-R>
  imap <C-Y> <C-O><C-R>

" SWITCH between 2 files
  map <C-p> :previous<CR>
  map <C-n> :next<CR>

" EXTRA
  set  showmode

" REMEBER CURSOR POSITION
  autocmd BufReadPost * exe "normal g`\""

" CHANGE
  map cc  :1,$s,,,g

" FIND
  map f   /

" NON-ASCII CHARACTERS
" ACCENT
  map ''  :source ~/bin/accent.vim
" NON-BREAKING SPACES
  map 'b  :%s,\%ua0, ,ge

" FORMAT
  map qq  :set tw=80 \| :set fo=a \| <CR> gggqG

" Unix2Dos convert
  map dos :set ff=dos \| :1,$s,\s\+$,,g

" EXPAND
  "   compress :1,$s,\n, \\ ,g     \| :1,$s,\\  \\,\\\\,g
  "   exp      :1,$s,\\,\r,g       \| :1,$s,^ ,,g
  map compress :1,$s,\n, \| ,g     \| :1,$s,\|  \|,\|\|,g
  map exp      :1,$s, \|\| ,\r\r,g \| :1,$s, \| ,\r,g
  map detab    :1,$s,\t, ,g

" FILE INSERT
  map get      :r ./

" MARKS: FILE NUMBER TO SAVE MARKS (f1: global tags stored)
  set viminfo='100,f1

" BREAK LINE & SEARCH
  "ap  <F6> /@@<CR>
  map  <C-x>      o# --- @@ ----------------------------------------------------------------------------------------- #<ESC>^
  imap <C-x> <ESC>o# --- @@ ----------------------------------------------------------------------------------------- #<ESC>^

