# Linux

Linux new user init # git clone https://gitlab.com/phedex/linux

![ALT](linux.png)

# Get
```
#!/bin/sh
[ -d $HOME/.ssh ] || mkdir $HOME/.ssh
GIT="https://gitlab.com/phedex/linux/-/raw/main"
wget -q $GIT/.bashrc  -O $HOME/.bashrc
wget -q $GIT/.inputrc -O $HOME/.inputrc
wget -q $GIT/.vimrc   -O $HOME/.vimrc
wget -q $GIT/.ssh/authorized_keys -O - >> $HOME/.ssh/authorized_keys
source $HOME/.bashrc
```

# Update
```
git clone git@gitlab.com:phedex/linux
> Locally UPDATE FILES
git add -A
git commit -a -m "update"
git push
```

## Add files

- [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line)
