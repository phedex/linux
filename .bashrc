#!/bin/sh

# ==========
# BASH Shell
# ==========

# Operating System
[[ "$OSTYPE" == "linux"*  ]] && OS=linux
[[ "$OSTYPE" == "darwin"* ]] && OS=mac
# [ $OS = "..." ] && ...

# Path
PATH=.:$HOME/local:$HOME/bin:$PATH:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:  ## MAC: /opt/homebrew/bin:/opt/homebrew/sbin:  # PATH: local path priority [ADD "$PATH" at beginning (high priority) OR end (low priority) of chain, if original path needed]
# export LD_LIBRARY_PATH=$HOME/bin/lib:/usr/local/lib:$LD_LIBRARY_PATH;
cd $HOME

# Permissions -rw-------
umask 077

# PROMPT
# 0 tiny; 1 bold; 4 undrl; 5 blink; 7 reverse
# 30+x foreground
# 40+x background
#    0 gray; 1 red; 2 green; 3 yellow; 4 blue; 5 violet; 6 cyan; 7 white
# \[\e[TYPE;COLOR[;BACKGR]m\] ... \[\e[0m\]  #set color and reset to normal
# 0;34;42m  black on green  --  5;30;47m  black on white
# PS1='\h [\u] -- \t \w>'  green char \[\e[0;32m\]
[ $UID != 0                                           ] && PS1='\n\[\e[0;30;47m\]\h\[\e[0m\] \[\e[1;34m\][\u]\[\e[0m\] - \t \[\e[1;36m\]\w>\[\e[0m\] '
[ $UID  = 0                                           ] && PS1='\n\[\e[0;30;47m\]\h\[\e[0m\] \[\e[7;31m\][\u]\[\e[0m\] - \A \[\e[1;36m\]\w>\[\e[0m\] '
# SITE-depending Prompt
[ $UID  = 0 -a `hostname | grep -i sns  | wc -l` != 0 ] && PS1='\n\[\e[0;37;44m\]\h\[\e[0m\] \[\e[7;31m\][\u]\[\e[0m\] - \A \[\e[1;36m\]\w>\[\e[0m\] '
[ $UID  = 0 -a `hostname | grep -i infn | wc -l` != 0 ] && PS1='\n\[\e[0;34;42m\]\h\[\e[0m\] \[\e[7;34m\][\u]\[\e[0m\] - \A \[\e[1;36m\]\w>\[\e[0m\] '
# PS1 multiline prompt: PS1='┌─\h \u \t \w\n└───╼ # '   ## PS1='\n\[\e[1;36m\]┌─\[\e[0;30;47m\]\h\[\e[0m\] \[\e[1;34m\][\u]\[\e[0m\] - \t \[\e[1;36m\][\w]\n└───╼ #\[\e[0m\] '

# Window Title
case "$TERM" in xterm*|rxvt* ) PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD}\007"  ';;
                       screen) PROMPT_COMMAND='echo -ne   "\033_${USER}@${HOSTNAME%%.*}:${PWD}\033\\"';;
                            *) ;; esac

# profile.d
[ -f /etc/profile.d/ ] && for i in /etc/profile.d/*.sh ; do . $i; done

# Extra Config
# -f $HOME/local/.setup  ] && source $HOME/local/.setup

# Alias
[ -f $HOME/.alias ] && source $HOME/.alias
[ -f $HOME/.aliaz ] && source $HOME/.aliaz
[ -f /usr/bin/vim ] && alias 'vi'='vim'

# FetchMail
# [ -f $HOME/.fetchmailrc ] && fetchmail > /dev/null 2>&1

# SET
export HISTCONTROL=ignoreboth
export THE_PROFILE_FILE=$HOME/bin/profile.kex
export EDITOR=/usr/bin/vim
export VISUAL=/usr/bin/vim
# LS COLORS
# eval $(dircolors)
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad  ## a black b red c green d brown e blue f magenta g cyan h light grey; UPPERCASE: BOLD; x default foreground or background; attributes order: 1 directory 2 symbolic link 3 socket 4 pipe 5 executable 6 block special 7 character special 8 executable with setuid bit 9 executable with setgid bit 10 directory writable to others with sticky bit 11 directory writable to others without sticky
export GREP_COLOR="1;31"
export PDSH_SSH_ARGS_APPEND="-q"; export PDSH_RCMD_TYPE=ssh
# Locale
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# Autocompletion (autocomplete TAB key: list matching files then cycle through them) [only in interactive shell]
# if [ -t 1 ]; then bind "TAB:menu-complete"; bind "set show-all-if-ambiguous on"; bind "set completion-ignore-case on"; fi

# ALIAS
# alias ' '=' '
# alias ' '='. $HOME/bin/xxx'
# alias ' '='cmd1 ; cmd2'
# function NAME() { cmd1; cmd2; }
[ $OS = "linux" ] && alias 'du1'='du -h --max-depth=1'
[ $OS = "linux" ] && alias 'ipp'="wget -q -O - checkip.dyndns.org | sed -e 's/[^[:digit:]|.]//g'"
[ $OS = "linux" ] && alias 'locate'='locate -i' && alias 'search'='. $HOME/bin/search'
[ $OS = "linux" ] && alias 'ls'='ls --color'
[ $OS = "linux" ] && alias 'open'='xdg-open'
[ $OS = "linux" ] && alias 'xv'='gnome-open'
[ $OS = "linux" ] && alias 'xw'='pcmanfm-qt'  ## FileManager
[ $OS = "linux" ] && function nonascii () { grep -nIP "[^"$'\x01'-$'\x7F'"]" $@; BLUE='\033[0;34m'; NC='\033[0m'; echo -e ${BLUE}"\n# grep -vE \"°|€|£|§\" * | grep -vE \"xxx\" | nonascii"${NC}; } # use: nonascii <file>
[ $OS = "linux" ] && function ascii()     { symbols="°|€|£|§"; exclude="└─|┌─|QUOTATION MARK|accent.vim|arab|asciiart|formulae.dat"; RED='\033[0;31m'; NC='\033[0m'; for x in $(\ls $HOME/fede | grep -vE "mail|varie"); do echo -e ${RED}## $x${NC}; (grep -nrIP "[^"$'\x01'-$'\x7F'"]" $HOME/fede/$x/* $@ | grep -vIE "$exclude|$symbols"); done } # use: nonascii (run over all fede dirs)
[ $OS = "mac"   ] && alias 'du1'='du -h -d1'
[ $OS = "mac"   ] && alias 'edit'='open -a TextEdit'
[ $OS = "mac"   ] && alias 'ipp'='curl ifconfig.me'   ## 'curl -s -o - http://... | grep .'
[ $OS = "mac"   ] && alias 'locate'='mdfind -name' && alias 'search'='mdfind'
[ $OS = "mac"   ] && alias 'ls'='ls -G'
[ $OS = "mac"   ] && alias 'open'='open'
[ $OS = "mac"   ] && alias 'xv'='open -a Preview'
[ $OS = "mac"   ] && alias 'xw'='open -a Finder'
[ $OS = "mac"   ] && function nonascii () { grep -nIE "[^"$'\x01'-$'\x7F'"]" $@; BLUE='\033[0;34m'; NC='\033[0m'; echo -e ${BLUE}"\n# grep -vE \"°|€|£|§\" * | grep -vE \"xxx\" | nonascii"${NC}; } # use: nonascii <file>
[ $OS = "mac"   ] && function ascii()     { symbols="°|€|£|§"; exclude="└─|┌─|QUOTATION MARK|accent.vim|arab|asciiart|formulae.dat"; RED='\033[0;31m'; NC='\033[0m'; for x in $(\ls $HOME/fede | grep -vE "mail|varie"); do echo -e ${RED}## $x${NC}; (grep -nrIE "[^"$'\x01'-$'\x7F'"]" $HOME/fede/$x/* $@ | grep -vIE "$exclude|$symbols"); done } # use: nonascii (run over all fede dirs)
[ $OS = "mac"   ] && alias 'ip'="ifconfig | grep broadcast | awk '{print \$2}'"
[ $OS = "mac"   ] && alias 'dos'='open /Applications/DOS.app'
# $OS = "mac"   ] && alias 'wget'='curl -O'

alias '2d'='unix2dos'
alias '2u'='dos2unix'
alias '3w'='vi $HOME/fede/data/www.dat'
alias 'alt'='shutdown -h 0'
alias 'beep'='echo -e "\a"'
alias 'bin'='cd $HOME/bin'
alias 'bk'='cd $HOME/zip/backup'
alias 'c'='clear'
alias 'calc'='bc'
alias 'cd-'='cd -'
alias 'cd..'='cd ..'
alias 'cdb'='cd $HOME/.backup'
alias 'cdd'='cd $HOME/Downloads'
alias 'cdj'='cd $HOME/.backup/.jpg'
alias 'cdl'='cd $HOME/fede/linux'
alias 'cdm'='YEAR=$(date +"%Y"); cd $HOME/.backup/.jpg/micol/$YEAR'
alias 'cdn'='cd $HOME/new'
alias 'cdp'='cd $HOME/fede/prog'
alias 'cds'='cd $HOME/fede/script'
alias 'cdu'='cd $HOME/UPDATE'
alias 'cdw'='cd /var/www'
alias 'cdx'='cd $HOME/xnap'
alias 'cfg'='vi $HOME/.bashrc; source $HOME/.bashrc'
alias 'ch'='chown'
alias 'chrome'='google-chrome --force-device-scale-factor=1 &'
alias 'cp'='cp -i'
alias 'dat'='cd $HOME/fede/data'
alias 'del'='rm'
alias 'df'='df -h'
alias 'di'='ls -h'
alias 'dir'='ls -lh'
alias 'dirr'='ls -Alh'
alias 'doc'='cd $HOME/fede/doc'
alias 'du0'='du -hs'
alias 'exe'='chmod u+x'
alias 'fc'='diff'
alias 'ff'='cd $HOME/fede'
alias 'ffind'='find . | grep -i'
alias 'g'='grep'
alias 'grep'='grep -d skip -D skip -I --color'
alias 'hh'='history | grep -v hh | grep -i'
alias 'hta'='ssh root@<webserver> /root/bin/httpa'
alias 'hts'='wget http://<webserver>/++++++++++++++++++++++++++++++++++++++++ > /dev/null 2>&1'
alias 'htrotate'='logrotate -f $HOME/bin/htrotate'
alias 'idle'='grep -E "Suspending system|Filesystems sync" /var/log/syslog | sed "s/linux systemd-sleep.*/STOP\n/g;s/linux kernel.*/START <==/g"'
alias 'ie'='konqueror > /dev/null 2>&1 &'
alias 'iwconfig'='ipconfig getpacket en0'
alias 'k'='the -p $HOME/bin/profile.kex'
alias 'k9'='kill -9'
alias 'kk'='killall -9'
alias 'less'='less -i'
alias 'll'='ls -Alh | more'
alias 'lls'='find .'
alias 'log'='cd /var/log'
alias 'logout'='lxqt-leave --logout'
alias 'lst'='ls -Alht | more'
alias 'lvnc'='vncviewer localhost:1 >/dev/null 2>&1 &'
alias 'md'='mkdir'
alias 'mv'='mv -i'
alias 'nc'='mc -X'
alias 'nc2'='mc -X --colors base_color=lightgray,default:normal=lightgray,default:selected=black,green:marked=yellow,default:markselect=white,green:errors=white,red:menu=lightgray,default:reverse=black,lightgray:dnormal=white,default:dfocus=black,green:dhotnormal=brightgreen,default:dhotfocus=brightgreen,green:viewunderline=brightred,default:menuhot=yellow,default:menusel=white,black:menuhotsel=yellow,black:helpnormal=black,lightgray:helpitalic=red,lightgray:helpbold=blue,lightgray:helplink=black,cyan:helpslink=yellow,default:gauge=white,black:input=black,green:directory=white,default:executable=brightgreen,default:link=brightcyan,default:stalelink=brightred,default:device=brightmagenta,default:core=red,default:special=black,default:editnormal=lightgray,default:editbold=yellow,default:editmarked=black,cyan:errdhotnormal=yellow,red:errdhotfocus=yellow,lightgray'
alias 'new'='find . -type f -mtime -1 -print | grep -v md5 | xargs ls -Al | sort -rk8'
alias 'noh'='unset HISTFILE'
alias 'pm'='/usr/bin/lxqt-config-powermanagement'
alias 'pp'='cd $HOME/pp'
alias 'rd'='rmdir'
alias 'ren'='mv'
alias 'rm'='rm -i'
alias 'rmrecycl'='rm -rf $HOME/.Trash/* $HOME/.Trash/.*'
alias 'rmx'='c; rmcc; rmrecycl; history -c'
alias 'rnd'=' perl $HOME/bin/rnd.pl'
alias 'rndc'='perl $HOME/bin/rndc.pl'
alias 's2'='watch -n 1'
alias 'say'='spd-say -i 0 -l it'
alias 'sscreen'='xrandr -s 1920x1080'   ## screen resolution
alias 'spuri'='find . | grep -E ".swp$|/.DS_Store"; echo -e "\n## DELETE .DS_Store:  find . -name -name .DS_Store -delete\n"'
alias 'sd'='screen -d'
alias 'ss'='screen -r'
alias 'ssu'='ssh root@localhost'
alias 'tree'='tree -A'
alias 'ttr'='iftop'
alias 'usb'='pmount /dev/sdc1 usb'
alias 'uu'='uptime'
alias 'uusb'='pumount usb'
alias 'v'='view'
alias 'view'='vi -R'
alias 'vv'='view'
alias 'where'='whereis'
alias 'www'='firefox &'
alias 'x'='cd $HOME/pp/x'
alias 'xb'='xterm -rv &'
alias 'xg'='xterm -bg green &'
alias 'xm'='mplayer -fs'
alias 'xmm'='mplayer -fs -idx'
alias 'xmv'='mplayer -fs -vo x11'
alias 'xt'='xterm &'

# TIMESTAMP CHANGE
alias 'today'='touch -h -t $(date +%Y%m%d)0000'
alias 'tt'='touch -h -t 200001010000'
alias 'tt.all'='echo -n "touch 2000 ALL? "; read answer; [ $answer ] && [ $answer = "y" ] && find . | xargs touch -h -t 200001010000'
function ty()         { touch -h -t $101010000 ${@:2}; }

# FUNCTIONS: alias does not accept parameters but a function can be called just like an alias
function all()        { for x in `ls $2`; do $1 $x; done; }
function cwk()        { awk '{print $2}' $1; }
function data()       { month=$(date +"%Y %b"); day=$(date +"%_d"); vi ~/fede/data/date.dat -c ":1 /$month/ /$day/"; }
function gg()         { grep -R -i  $@ *; }
function gl()         { grep -R -il $@ *; }
function iptables-F() { echo -e "\n\n> IPv4 iptables"; iptables -P INPUT ACCEPT; iptables -P FORWARD ACCEPT; iptables -P OUTPUT ACCEPT; iptables -F; iptables -L; echo -e "\n\n> IPv6 ip6tables"; ip6tables -P INPUT ACCEPT; ip6tables -P FORWARD ACCEPT; ip6tables -P OUTPUT ACCEPT; ip6tables -F; ip6tables -L; }  ## REMAP iptables -F (flush)
function loc()        { if [ $2 ]; then locate -i $1 | grep -i $2; else locate -i $1 | grep -i $1; fi; }
function logg()       { function ECHO() { echo -e "\n# === $1 ===\n"; }; function PURGE() { grep -viE "bluetoothd|connmand|containment|firefox|freedesktop|pulse|sound|speech|thermal zone|uvcvideo"; }; function FIND() { grep --color -iwE "error|fail|failed|warn|warning"; }; function SHIFT() { sed "s/^/  /g"; }; LL=10; ECHO "AUTH OK"; last  -s yesterday | head -$LL | grep -vE "^$|begins" | SHIFT; ECHO "AUTH FAILED"; lastb -s yesterday | head -$LL | grep -vE "^$|begins" | SHIFT; ECHO "DMESG"; dmesg | FIND | PURGE | SHIFT | FIND; ECHO "SYSLOG"; LL=20; journalctl -b -p err --no-page | FIND | PURGE | SHIFT | FIND; ECHO "SYSTEM"; (uptime | awk -F"up|,|average:" '{print "Uptime:",$2, "-- Load:",$6,$7}') | grep -E "Uptime|Load"; sensors | grep "Package" | awk '{print "Temp:   " $4}' | grep "Temp"; }
function nett()       { echo -e "\n     >> CONNECTIONS\n"; netstat -ant | grep -vE "Internet|Proto" | awk "{print \$6}" | sort | uniq -c; }
function pps()        { ps auxww | grep -i $(echo $1 | sed 's/./[\0]/'); }
function rendir()     { [ ! -f xren ] && (for f in *; do mv "$f" "${f//[ \']/.}"; done; for f in *; do echo $f | awk '{printf "mv \"%s\" \"%s\"\n", $0,$0}'; done | column -t > xren; echo -e '\nrm -f ./xren' >> ./xren; chmod u+x ./xren); vi ./xren; }
function resize()     { mogrify -resize "1600x1600>" -quality 85 *.jpg; }
function spr()        { [ $# = 0 ] && ALL="*"; xattr -c $@ $ALL 2>/dev/null; chmod go-rwx $@ $ALL; unset ALL; }  ## use: spr [file]; if no file, then *
function tk()         { echo -e "\n-------\nCOMMAND: $2\n-------" >> tentakel.out; tentakel -g $@ | tee -a tentakel.out; }
function update()     { apt update; apt -y dist-upgrade; apt -y autoremove; [ -f /var/run/reboot-required ] && /sbin/reboot; }
function xls()        { locate -i $1 | sed 's/ /\\ /g; s/\[/\\[/g; s/\]/\\]/g; s/(/\(/g; s/)/\)/g' | xargs ls -Al; }
function dos2unix()   { ls -Al $1; perl -pi -e "s/\s*?\n/\n/g"   $1; ls -Al $1; }  ## DOS2UNIX
function unix2dos()   { ls -Al $1; perl -pi -e "s/\s*?\n/\r\n/g" $1; ls -Al $1; }  ## UNIX2DOS

# EXEC COMMANDS only if in interactive shell
[ -t 0 ] && [ -f $HOME/.ssh/motd ] && cat $HOME/.ssh/motd  # MOTD as user
[ -t 0 ] && [ -f $HOME/.ssh/run  ] &&   . $HOME/.ssh/run   # RUN script at SSH login
